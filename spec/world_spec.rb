require 'spec_helper'
require './app/world'
require './app/alive'
require './app/dead'
# require 'ruby-debug'

describe World do
  let(:world) {World.new}


  before do
      world.add_cell(cell)

      cell.my_neighbours.first(neighbour_count).each do |x, y|
        world.add_cell(Alive.new(world, x, y))
      end

      cell.my_neighbours.last(8 - neighbour_count).each do |x, y|
        world.add_cell(Dead.new(world, x, y))
      end
  end

  context 'live cells' do
    let(:cell) { Alive.new(world, 1, 1) }
    context 'no live cells' do
      let(:neighbour_count) { 0 }

      it 'dies of loneliness' do
        world.tick
        expect(world.cell_at(cell.x, cell.y)).to be_a Dead
      end
    end

    context '1 live cell' do
      let(:neighbour_count) { 1 }
      it 'dies of loneliness' do
        world.tick
        expect(world.cell_at(cell.x, cell.y)).to be_a Dead
      end
    end

    context '2 live cell' do
      let(:neighbour_count) { 2 }
      
      it 'continues to be live' do
        world.tick
        expect(world.cell_at(cell.x, cell.y)).to be_a Alive
      end
    end

    context '3 live cells' do
      let(:neighbour_count) { 3 }

      it 'continues to live' do
        world.tick
        expect(world.cell_at(cell.x, cell.y)).to be_a Alive
      end
    end

    context '4 live cells' do
      let(:neighbour_count) { 4 }

      it 'dies of overcrowding' do
        world.tick
        expect(world.cell_at(cell.x, cell.y)).to be_a Dead
      end
    end
  end

  context 'Dead cells' do
    let(:cell) { Dead.new(world, 1, 1) }
    
    context 'no live cells' do
      let(:neighbour_count) { 0 }

      it 'dies of loneliness' do
        world.tick
        expect(world.cell_at(cell.x, cell.y)).to be_a Dead
      end
    end

    context '1 live cell' do
      let(:neighbour_count) { 1 }
      it 'dies of loneliness' do
        world.tick
        expect(world.cell_at(cell.x, cell.y)).to be_a Dead
      end
    end

    context '2 live cell' do
      let(:neighbour_count) { 2 }
      
      it 'continues to be dead' do
        world.tick
        expect(world.cell_at(cell.x, cell.y)).to be_a Dead
      end
    end

    context '3 live cells' do
      let(:neighbour_count) { 3 }

      it 'comes back to life' do
        world.tick
        expect(world.cell_at(cell.x, cell.y)).to be_a Alive
      end
    end

    context '4 live cells' do
      let(:neighbour_count) { 4 }

      it 'continues to be dead' do
        world.tick
        expect(world.cell_at(cell.x, cell.y)).to be_a Dead
      end
    end
  end
end