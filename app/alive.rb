require './app/cell'
class Alive < Cell

  def change_state
    if self.alive_neighbours.count < 2 || self.alive_neighbours.count > 3
      self.replace(Dead.new(@world, @x, @y))
    else
      self
    end
  end

end