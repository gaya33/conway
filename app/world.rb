class World
  attr_accessor :cells
  
  def initialize
    @cells = []
  end

  def add_cell(cell)
    @cells << cell
  end

  def cell_at(x, y)
    @cells.detect { |cell| cell.x == x && cell.y == y}
  end

  def tick
    @cells.map(&:change_state)
  end
  
end