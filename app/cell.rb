class Cell
  attr_accessor :world, :x, :y

  def initialize(world, x, y)
    @world, @x, @y = world, x, y  
  end

  def replace(new_cell)
    @world.cells[@world.cells.index(self)] = new_cell
  end

  def my_neighbour_cells
    my_neighbours.map { |x, y| world.cell_at(x, y) }.compact
  end

  def alive_neighbours
    my_neighbour_cells.select{ |cell| cell.is_a?(Alive)}  
  end
  
  def my_neighbours
    neighbouring_cells = [x - 1, x, x + 1].product [y - 1, y, y + 1]
    neighbouring_cells.delete([x,y])
    neighbouring_cells
  end

end