require './app/cell'
class Dead < Cell

  def change_state
    if self.alive_neighbours.count == 3
      self.replace(Alive.new(@world, @x, @y))
    else
      self
    end
  end
end